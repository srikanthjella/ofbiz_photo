package restcomponent;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.ofbiz.base.json.JSONWriter;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.DelegatorFactory;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceDispatcher;
import org.ofbiz.service.ServiceUtil;

import javolution.util.FastMap;

@Path("/invokeResource")
public class PingResource {

	@Context
	HttpHeaders headers;

	@POST
	@Produces("text/json")
	@Consumes("application/x-www-form-urlencoded")
	@Path("{serviceName}")
    public Response invokeSerivcePostMethod( @PathParam("serviceName") String serviceName, 
    		MultivaluedMap<String, String> formParams) {
    	
		String username = null;
		String password = null;
		
		try {
			username = headers.getRequestHeader("login.username").get(0);
			password = headers.getRequestHeader("login.password").get(0);
		} catch (NullPointerException e) {
//			return Response.serverError().entity("Problem reading http header(s): login.username or login.password").build();
		}
		
    	GenericDelegator delegator = (GenericDelegator) DelegatorFactory.getDelegator("default");
    	LocalDispatcher dispatcher = ServiceDispatcher.getLocalDispatcher("default",delegator);

    	Map<String, String> paramMap = UtilMisc.toMap(
    			"login.username", username,
    			"login.password", password
    		);
		Map<String, Object> result = FastMap.newInstance();
		
		try {
			for (String key : formParams.keySet()) {
				System.out.println( "Key Value Pair: " + key + " = " +  URLDecoder.decode( formParams.get(key).get(0), "UTF-8"));
				
				if( key.startsWith("__")) {
					paramMap.put(key.substring(2), URLDecoder.decode( formParams.get(key).get(0), "UTF-8"));
				}
				else if( username == null && "login.username".equals(key)) {
					paramMap.put(key, URLDecoder.decode( formParams.get(key).get(0), "UTF-8"));
				}
				else if( password == null && "login.password".equals(key)) {
					paramMap.put(key, URLDecoder.decode( formParams.get(key).get(0), "UTF-8"));
				}
			}
		
			result = dispatcher.runSync( serviceName, paramMap);
		} catch (GenericServiceException e1) {
			Debug.logError(e1, PingResource.class.getName());
			return Response.serverError().entity(e1.toString()).build();
		} catch (UnsupportedEncodingException e) {
			Debug.logError(e, PingResource.class.getName());
			return Response.serverError().entity(e.toString()).build();
		}
		
		if (ServiceUtil.isSuccess(result)) {
			
			for (String key : result.keySet()) {
//				Below line is needed. The first argument in replaceAll is 
//				NOT a space but a unicode character 160
//				if this file does not compile with below line error
//				then save this file in UTF-8 format. Default is Cp1252 format.
				result.put(key, result.get(key).toString().replaceAll(" ",""));
			}
			
            StringWriter sw = new StringWriter();
            JSONWriter jsonWriter;
            jsonWriter = new JSONWriter(sw);
            try {
				jsonWriter.write( result);
			} catch (IOException e) {
				return Response.serverError().entity( "jsonWriter.write error - " + e.getMessage()).build();
			}
            
			return Response.ok( sw.toString()).type(MediaType.APPLICATION_JSON).build();
		}
		else if (ServiceUtil.isError(result) || ServiceUtil.isFailure(result)) {
			return Response.serverError().entity(ServiceUtil.getErrorMessage(result)).build();
		}
		
		// shouldn't ever get here ... should we?
		throw new RuntimeException("Invalid ");
    }
	
	@GET
	@Produces("text/json")
	@Consumes("application/x-www-form-urlencoded")
	@Path("{serviceName}")
	public Response invokeSerivceGetMethod(@PathParam("serviceName") String serviceName, 
    		MultivaluedMap<String, String> formParams) {
		return invokeSerivcePostMethod(serviceName, formParams);
	}
}
